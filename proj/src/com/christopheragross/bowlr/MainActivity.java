package com.christopheragross.bowlr;

import java.util.List;

import com.christopheragross.bowlr.R;
import com.christopheragross.bowlr.adapters.BowlrMenuItemAdapter;
import com.christopheragross.bowlr.db.BowlrDataSource;
import com.christopheragross.bowlr.db.BowlrDbOpenHelper;
import com.christopheragross.bowlr.menu.BowlrMenu;
import com.christopheragross.bowlr.menu.BowlrMenuItem;

import android.app.ListActivity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

public class MainActivity extends ListActivity {

   private static final int REQUEST_CODE = 0x01;
   List<BowlrMenuItem> bowlrmenu;
   
   private BowlrDataSource dataSource;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);

      dataSource = new BowlrDataSource(this);
      
      bowlrmenu = new BowlrMenu(this).getMenu();
      BowlrMenuItemAdapter adapter = new BowlrMenuItemAdapter(this,
            R.layout.item_bowlrmenuitem, bowlrmenu);
      setListAdapter(adapter);
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.main, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();
      if (id == R.id.action_settings) {
         return true;
      }
      return super.onOptionsItemSelected(item);
   }

   @Override
   protected void onListItemClick(ListView l, View v, int position, long id) {
      super.onListItemClick(l, v, position, id);

      BowlrMenuItem menuItem = bowlrmenu.get(position);
      Intent intent = new Intent(this, menuItem.getActivityClass());
      startActivityForResult(intent, REQUEST_CODE);
   }
   
   @Override
   protected void onResume()
   {
      super.onResume();
      dataSource.open();
   }
   
   @Override
   protected void onPause()
   {
      super.onPause();
      dataSource.close();
   }

}
