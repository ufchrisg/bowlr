package com.christopheragross.bowlr.exception;

public class BowlableException extends Exception
{

   /**
    * 
    */
   private static final long serialVersionUID = 4753916029931190719L;
   
   public BowlableException()
   {
      super();
   }
   
   public BowlableException(String message)
   {
      super(message);
   }

}
