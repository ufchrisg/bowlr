package com.christopheragross.bowlr.exception;

public class BowlerException extends Exception
{

   /**
    * 
    */
   private static final long serialVersionUID = 4344104978449625280L;

   public BowlerException()
   {
      super();
   }
   
   public BowlerException(String message)
   {
      super(message);
   }
   
}
