package com.christopheragross.bowlr.exception;

public class PinDeckException extends Exception
{

   /**
    * 
    */
   private static final long serialVersionUID = 6181480165421880465L;

   public PinDeckException()
   {
      super();
   }
   
   public PinDeckException(String message)
   {
      super(message);
   }
}
