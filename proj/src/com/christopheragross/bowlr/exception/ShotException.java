package com.christopheragross.bowlr.exception;

public class ShotException extends Exception
{

   /**
    * 
    */
   private static final long serialVersionUID = -4008564567211951847L;

   public ShotException()
   {
      super();
   }
   
   public ShotException(String message)
   {
      super(message);
   }
}
