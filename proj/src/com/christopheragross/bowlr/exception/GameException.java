package com.christopheragross.bowlr.exception;

public class GameException extends Exception
{

   /**
    * 
    */
   private static final long serialVersionUID = 1800700406700716909L;

   public GameException()
   {
      super();
   }
   
   public GameException(String message)
   {
      super(message);
   }
   
}
