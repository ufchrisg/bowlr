/**
 * Exception classes.
 * 
 * @since 1.0
 * @author Christopher A Gross
 * @version 1.0
 */
package com.christopheragross.bowlr.exception;