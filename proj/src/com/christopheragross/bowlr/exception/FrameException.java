package com.christopheragross.bowlr.exception;

public class FrameException extends Exception
{

   /**
    * 
    */
   private static final long serialVersionUID = 2464901338556315415L;

   public FrameException()
   {
      super();
   }
   
   public FrameException(String message)
   {
      super(message);
   }
   
}
