package com.christopheragross.bowlr.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class BowlrDataSource
{
   
   private static final String kLogTag = "BOWLR";
   
   private SQLiteOpenHelper dbHelper;
   private SQLiteDatabase database;

   public BowlrDataSource(Context context)
   {
      dbHelper = new BowlrDbOpenHelper(context);
   }
   
   public void open()
   {
      database = dbHelper.getWritableDatabase();
      Log.i(kLogTag, "Database opened");
   }
   
   public void close()
   {
      database.close();
      Log.i(kLogTag, "Database closed");
   }
   
}
