package com.christopheragross.bowlr.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class BowlrDbOpenHelper extends SQLiteOpenHelper 
{

   private static final String kLogTag = "BOWLR";

   private static final String kDatabaseName = "bowlr.db";
   private static final int kDatabaseVersion = 3;

   private static final String kCreateTable = "CREATE TABLE {TABLE} ({ROWS})";

   // table name constants
   public static final String kTableSession = "SESSION";
   public static final String kTableSessionType = "SESSIONTYPE";
   public static final String kTableSessionRules = "SESSIONRULES";
   public static final String kTableGame = "GAME";
   public static final String kTableFrame = "FRAME";
   public static final String kTableShot = "SHOT";
   public static final String kTableBowler = "BOWLER";
   public static final String kTableBall = "BALL";
   public static final String kTableLocation = "LOCATION";
   
   // GAME
   public static final String kColumnGameId = "gameId";
   public static final String kColumnGameNumFrames = "numFrames";
   
   // FRAME
   public static final String kColumnFrameId = "frameId";
   public static final String kColumnFrameNumber = "frameNumber";
   
   // SHOT
   public static final String kColumnShotId = "shotId";
   public static final String kColumnShotNextShotId = "nextShotId";
   public static final String kColumnShotStanceBoard = "stanceBoard";
   public static final String kColumnShotTargetBoard = "targetBoard";
   public static final String kColumnShotTargetHitBoard = "targetHitBoard";
   public static final String kColumnShotBreakPointBoard = "breakPointBoard";
   public static final String kColumnShotBreakPointHitBoard = 
    "breakPointHitBoard";
   public static final String kColumnShotEntryPointBoard = "entryPointBoard";
   public static final String kColumnShotPinsHit = "pinsHit";
   
   // BOWLER
   public static final String kColumnBowlerId = "bowlerId";
   public static final String kColumnBowlerFirstName = "firstName";
   public static final String kColumnBowlerLastName= "lastName";
   public static final String kColumnBowlerMemberId = "memberId";
   public static final String kColumnBowlerDefaultHand = "defaultHand";
   
   // BALL
   public static final String kColumnBallId = "ballId";
   public static final String kColumnBallName = "name";
   public static final String kColumnBallManufacturer = "manufacturer";

   // Enum holding all tables to create (only on initial app open)
   private enum BowlrTableSql 
   {
      BOWLER
      (
       kColumnBowlerId + " INTEGER PRIMARY KEY AUTOINCREMENT",
       kColumnBowlerFirstName + " TEXT NOT NULL DEFAULT 'Bowler'",
       kColumnBowlerLastName + " TEXT",
       kColumnBowlerMemberId + " TEXT UNIQUE",
       kColumnBowlerDefaultHand + " INTEGER NOT NULL DEFAULT 0"
      ),
      BALL
      (
       kColumnBallId + " INTEGER PRIMARY KEY AUTOINCREMENT",
       kColumnBallName + " TEXT NOT NULL",
       kColumnBallManufacturer + " TEXT"
      ),
      GAME
      (
       kColumnGameId + " INTEGER PRIMARY KEY AUTOINCREMENT",
       kColumnBowlerId + " INTEGER NOT NULL DEFAULT 0",
       kColumnGameNumFrames + " INTEGER NOT NULL DEFAULT 10",
       "FOREIGN KEY(" + kColumnBowlerId + ") REFERENCES BOWLER(" +
        kColumnBowlerId + ") ON DELETE SET DEFAULT ON UPDATE CASCADE"
      ),
      FRAME
      (
       kColumnFrameId + " INTEGER PRIMARY KEY AUTOINCREMENT",
       kColumnGameId + " INTEGER NOT NULL",
       kColumnFrameNumber + " INTEGER",
       "FOREIGN KEY(" + kColumnGameId + ") REFERENCES GAME(" + kColumnGameId + 
        ") ON DELETE CASCADE ON UPDATE CASCADE"
      ),
      SHOT
      (
       kColumnShotId + " INTEGER PRIMARY KEY AUTOINCREMENT",
       kColumnShotNextShotId + " INTEGER",
       kColumnFrameId + " INTEGER NOT NULL",
       kColumnBallId + " INTEGER NOT NULL DEFAULT 0",
       kColumnShotStanceBoard + " REAL",
       kColumnShotTargetBoard + " REAL",
       kColumnShotTargetHitBoard + " REAL",
       kColumnShotBreakPointBoard + " REAL",
       kColumnShotBreakPointHitBoard + "REAL",
       kColumnShotEntryPointBoard + " REAL",
       kColumnShotPinsHit + " INTEGER NOT NULL DEFAULT 0",
       "FOREIGN KEY(" + kColumnShotNextShotId + ") REFERENCES SHOT(" +
        kColumnShotId + ") ON DELETE SET NULL ON UPDATE CASCADE",
       "FOREIGN KEY(" + kColumnBallId + ") REFERENCES BALL(" +
        kColumnBallId + ") ON DELETE SET DEFAULT ON UPDATE CASCADE",
       "FOREIGN KEY(" + kColumnFrameId + ") REFERENCES FRAME(" +
        kColumnFrameId + ") ON DELETE CASCADE ON UPDATE CASCADE"
      );
      
      private final String[] rows;
      BowlrTableSql(String... rows)
      {
         this.rows = rows;
      }
      
      protected String getRowsAsSingleString()
      {
         StringBuilder rowStringBuilder = new StringBuilder();
         for (int i = 0; i < this.rows.length; ++i)
         {
            rowStringBuilder.append(this.rows[i]);
            if (i + 1 < this.rows.length)
            {
               rowStringBuilder.append(", ");
            }
         }
         return rowStringBuilder.toString();
      }
   }
   
   
   private static final String[] kInitialRows =
   {
      "INSERT INTO " + kTableBowler + " (" + kColumnBowlerId + ", " +
       kColumnBowlerFirstName + ") VALUES (0, 'Bowler')",
      "INSERT INTO " + kTableBall + " (" + kColumnBallId + ", " +
       kColumnBallName + ") VALUES (0, 'Default Ball')"
   };
   
   private enum BowlrTableUpdates 
   {
      version2
      (
       "ALTER TABLE " + kTableShot + " ADD COLUMN " + 
        kColumnShotTargetHitBoard + " REAL",
       "ALTER TABLE " + kTableShot + " ADD COLUMN " +
        kColumnShotBreakPointHitBoard + " REAL"
      ),
      version3
      (
       "DROP TABLE IF EXISTS LOCATION",
       "DROP TABLE IF EXISTS SESSIONRULES",
       "DROP TABLE IF EXISTS SESSIONTYPE",
       "DROP TABLE IF EXISTS SESSION",
       "BEGIN TRANSACTION",
       "CREATE TEMPORARY TABLE " + kTableGame + "_backup (" +
        kColumnGameId + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
        kColumnBowlerId + " INTEGER NOT NULL DEFAULT 0, " +
        kColumnGameNumFrames + " INTEGER NOT NULL DEFAULT 10, " +
        "FOREIGN KEY(" + kColumnBowlerId + ") REFERENCES BOWLER(" +
        kColumnBowlerId + ") ON DELETE SET DEFAULT ON UPDATE CASCADE)",
       "INSERT INTO " + kTableGame + "_backup SELECT " + kColumnGameId + ", " +
        kColumnBowlerId + ", " + kColumnGameNumFrames + " FROM " + kTableGame,
       "DROP TABLE " + kTableGame,
       "CREATE TABLE " + kTableGame + " (" +
        kColumnGameId + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
        kColumnBowlerId + " INTEGER NOT NULL DEFAULT 0, " +
        kColumnGameNumFrames + " INTEGER NOT NULL DEFAULT 10, " +
        "FOREIGN KEY(" + kColumnBowlerId + ") REFERENCES BOWLER(" +
        kColumnBowlerId + ") ON DELETE SET DEFAULT ON UPDATE CASCADE)",
       "DROP TABLE " + kTableGame + "_backup",
       "COMMIT"
      );
      
      private final String[] rows;
      BowlrTableUpdates(String... rows)
      {
         this.rows = rows;
      }
      
      protected String[] getRows()
      {
         return this.rows;
      }
   }
   
   public BowlrDbOpenHelper(Context context) 
   {
      super(context, kDatabaseName, null, kDatabaseVersion);
   }

   @Override
   public void onCreate(SQLiteDatabase db) 
   {
      String sql;
      
      // create tables based on enum object
      for (BowlrTableSql table : BowlrTableSql.values()) 
      {
         sql = kCreateTable.replace("{TABLE}", table.toString())
          .replace("{ROWS}", table.getRowsAsSingleString());
         //Log.d(kLogTag, sql);
         db.execSQL(sql);
      }
      
      // populate some initial rows that are necessary
      for (String initialRow : kInitialRows) {
         //Log.d(kLogTag, initialRow);
         db.execSQL(initialRow);
      }
      
      Log.i(kLogTag, "Database tables created.");
   }

   @Override
   public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
   {
      // for every version up to and including newVersion beyond oldVersion
      // run the SQL found in the BowlrTableUpdates enum
      for (int i = oldVersion+1; i <= newVersion; ++i) 
      {
         BowlrTableUpdates bowlrTableUpdates = 
          BowlrTableUpdates.valueOf("version"+i);
         
         // this should never be null as version number should only
         // be changed if there are changes to the database, but just in case
         if (null == bowlrTableUpdates)
         {
            Log.w(kLogTag, 
             "Found an empty update in enum for 'version"+i+"'");
            continue;
         }
         
         // get the rows then run each
         String[] sqlUpdates = bowlrTableUpdates.getRows();
         for (String sqlUpdate : sqlUpdates)
         {
            db.execSQL(sqlUpdate);
         }
         
         Log.i(kLogTag, "Database upgraded from version "+(i-1)+
          " to "+i);
      }
   }

}
