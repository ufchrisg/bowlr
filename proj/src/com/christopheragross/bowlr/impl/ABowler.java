package com.christopheragross.bowlr.impl;

import java.util.Random;

import com.christopheragross.bowlr.api.Bowler;
import com.christopheragross.bowlr.api.Storable;
import com.christopheragross.bowlr.exception.BowlerException;

public class ABowler implements Bowler, Storable
{
   // DEFINE  
   private Handedness hand;
   private String name;
   private String memberId;
   
   private int databaseId = 0;
   
   private boolean dbWriteEnabled = true;
   
   public ABowler(String name) throws BowlerException
   {
      if (name == null || name == "")
      {
         throw new BowlerException("Attempted to create bowler with no name");
      }
      this.name = name;
      
      // member id not necessary so default to null
      this.memberId = null;
      
      // more people are right-handed so let's default to right
      this.hand = Handedness.RIGHT;
      
      // TODO: Connect to database and get databaseId for this "name"
      // Create a new entry and return the ID if no Bowler found
      this.databaseId = 0;
   }
   
   // ACCESSORS -- get properties
   
   @Override
   public String getMemberId()
   {
      return this.memberId;
   }
   
   @Override
   public String getName()
   {
      return this.name;
   }
   
   // ACCESSORS -- get property values
   
   @Override
   public boolean isDbWriteEnabled()
   {
      return this.dbWriteEnabled;
   }
   
   @Override
   public boolean isLefty()
   {
      return this.hand == Handedness.LEFT;
   }
   
   @Override
   public boolean isRighty()
   {
      return this.hand == Handedness.RIGHT;
   }
   
   @Override
   public boolean isTwoHand()
   {
      return this.hand == Handedness.TWO;
   }
   
   // ACCESSORS -- set property values
   
   @Override
   public void enableDbWrite()
   {
      this.dbWriteEnabled = true;
   }

   @Override
   public void disableDbWrite()
   {
      this.dbWriteEnabled = false;
   }
   
   @Override
   public void removeMemberId()
   {
      if (this.memberId == null || this.memberId == "")
      {
         return;
      }
      
      this.setMemberId(null);
   }
   
   @Override
   public void setMemberId(String newMemberId)
   {
      if (newMemberId.equals(this.memberId))
      {
         return;
      }
      
      if (this.dbWriteEnabled)
      {
         // TODO: Call database to set/update memberId
      }
      
      this.memberId = newMemberId;
   }
   
   @Override
   public void makeLefty()
   {
      if (this.isLefty())
      {
         return;
      }
      
      if (this.dbWriteEnabled)
      {
         // TODO: Update db
      }
      
      this.hand = Handedness.LEFT;
   }
   
   @Override
   public void makeRighty()
   {
      if (this.isRighty())
      {
         return;
      }
      
      if (this.dbWriteEnabled)
      {
         // TODO: Update db
      }
      
      this.hand = Handedness.RIGHT;
   }
   
   @Override
   public void makeTwoHand()
   {
      if (this.isTwoHand())
      {
         return;
      }
      
      if (this.dbWriteEnabled)
      {
         // TODO: Update db
      }
      
      this.hand = Handedness.TWO;
   }   
   



   
   // CONDUCT BUSINESS
   
   @Override
   public void setName(String newName) throws BowlerException
   {
      // logic tests
      if (newName == null || !newName.matches("^.+$"))
      {
         // Empty or null String found for newName
         throw new BowlerException("Attempted to set an empty name");
      }
      if (newName.equals(this.name))
      {
         // no change so just return
         return;
      }
      
      // business rule tests
      this.testSetName(newName);
      
      if (this.databaseId == 0 && !Bowler.kDefaultName.equals(newName))
      {
         if (this.dbWriteEnabled)
         {
            // databaseId is default but newName is not so create new bowler
            // TODO: Call database to create new bowler
         }
      }
      if (this.databaseId != 0)
      {
         if (this.dbWriteEnabled)
         {
            // TODO: Call database to update name
         }
      }
      
      // set the name
      this.doSetName(newName);
   }
   
   /**
    * Tests the business rules when attempting to set the name of a
    *  Bowler. In this case, the Bowler name must be less than 100 characters.
    * @param newName
    * @throws BowlerException
    */
   public void testSetName(String newName) throws BowlerException
   {
      if (newName.length() > 99)
      {
         throw new BowlerException("Bowler name cannot exceed 99 characters");
      }
   }
   
   /**
    * Sets the name but skips logic and business rule tests. This method
    *  should only be used when loading data from persistent storage or
    *  after running through the logic and business rule tests.
    * @param newName
    */
   public void doSetName(String newName)
   {
      this.name = newName;
   }
   
   /**
    * Check for equality between this object and another
    * @return true if this object is equal to the passed object
    */
   public boolean equals(Object obj)
   {
      if (obj instanceof Bowler)
      {
         Bowler other = (Bowler)obj;
         if (!this.name.equals(other.getName()))
         {
            return false;
         }
         if (!this.memberId.equals(other.getMemberId()))
         {
            return false;
         }
         return true;
      }
      return false;
   }
   
   /**
    * Prints the properties of this Bowler
    */
   public String toString()
   {
      StringBuilder bowlerString = new StringBuilder();
      bowlerString.append("Bowler: " + this.name + "\n");
      bowlerString.append(" member id: " + this.memberId + "\n");
      bowlerString.append(" handedness: ");
      if (this.isLefty())
      {
         bowlerString.append("Left");
      }
      if (this.isRighty())
      {
         bowlerString.append("Right");
      }
      if (this.isTwoHand())
      {
         bowlerString.append("Two-Hand");
      }
      return bowlerString.toString();
   }
   
   /**
    * Create a test Bowler
    * @return Bowler object for testing
    */
   public static ABowler testBowler() throws BowlerException
   {
      ABowler testBowler = new ABowler("Bob Bowler");
      
      // disable database writing for test object
      testBowler.disableDbWrite();

      return testBowler;
   }
   
   public static void main(String[] args)
   {
      Bowler bowler = null;
      int successCount = 0;
      int failCount = 0;
      
      System.out.println("## Bowler tests");
      
      // test creating a bowler
      System.out.println("# Creating a bowler");
      try
      {
         System.out.print("Creating bowler Bob Bowler... ");
         bowler = ABowler.testBowler();
      }
      catch (BowlerException be)
      {
         System.out.println("!FATAL: " + be.getMessage());
         return;
      }
      
      if ("Bob Bowler".equals(bowler.getName()))
      {
         System.out.println("SUCCESS");
         ++successCount;
      }
      else
      {
         System.out.println("FAIL");
         ++failCount;
      }
      
      // test changing name
      System.out.println("# Changing name");
      String newBowlerName = "Betty Bowler";
      try
      {
         System.out.print("Changing to " + newBowlerName + "... ");
         bowler.setName(newBowlerName);
      } 
      catch (BowlerException be)
      {
         System.out.println("FATAL: " + be.getMessage());
         return;
      }
      
      if (newBowlerName.equals(bowler.getName()))
      {
         System.out.println("SUCCESS");
         ++successCount;
      }
      else
      {
         System.out.println("FAIL");
         ++failCount;
      }
      
      // test setting/changing member id
      System.out.println("# Setting & changing member id");
      Random rand = new Random();
      String newMemberId = 
       (rand.nextInt(9900)+100) + "-" + (rand.nextInt(9000)+1000);
      System.out.print("Setting member id to " + newMemberId + "... ");
      try 
      {
         bowler.setMemberId(newMemberId);
      }
      catch (BowlerException be)
      {
         System.out.println("FATAL: " + be.getMessage());
      }
      if (newMemberId.equals(bowler.getMemberId()))
      {
         System.out.println("SUCCESS");
         ++successCount;
      }
      else
      {
         System.out.println("FAIL");
         ++failCount;
      }
      
      // test setting/changing handedness
      System.out.println("# Setting & changing handedness");
      System.out.print("Making bowler left-handed... ");
      bowler.makeLefty();
      if (bowler.isLefty())
      {
         System.out.println("SUCCESS");
         ++successCount;
      }
      else
      {
         System.out.println("FAIL");
         ++failCount;
      }
      System.out.print("Making bowler right-handed... ");
      bowler.makeRighty();
      if (bowler.isRighty())
      {
         System.out.println("SUCCESS");
         ++successCount;
      }
      else
      {
         System.out.println("FAIL");
         ++failCount;
      }
      System.out.print("Making bowler two-handed... ");
      bowler.makeTwoHand();
      if (bowler.isTwoHand())
      {
         System.out.println("SUCCESS");
         ++successCount;
      }
      else
      {
         System.out.println("FAIL");
         ++failCount;
      }
      
      // print final bowler
      System.out.println("# Final object");
      System.out.println(bowler);
      
      // test results
      System.out.println("# Test Results");
      System.out.print("Total: " + (successCount + failCount) + " | ");
      System.out.print("Successes: " + successCount + " | ");
      System.out.println("Failures: " + failCount);
   }


}
