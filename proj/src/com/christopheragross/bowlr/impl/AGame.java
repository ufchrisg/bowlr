package com.christopheragross.bowlr.impl;

import java.util.ArrayList;
import java.util.Date;

import com.christopheragross.bowler.api.Game;
import com.christopheragross.bowlr.api.Bowler;
import com.christopheragross.bowlr.api.Storable;
import com.christopheragross.bowlr.exception.GameException;

public class AGame implements Game, Storable, Bowlable
{
   public static final String kTypeBaker = "Baker";
   public static final String kTypeNoTap = "NoTap";

   private Date dateTime;
   private GameRules rules = new GameRules();
   private String status;
   private String type;
   
   private ArrayList<Bowler> bowlers;
   private ArrayList<Frame> frames;
   
   // CONSTRUCTORS
   
   public AGame(Bowler bowler) throws GameException
   {
      this.addBowler(bowler);
      this.createFrame();
   }

   
   // ACCESSORS -- get properties
   
   public String getDate()
   {
      return this.dateTime.toString();
   }
   
   // ACCESSORS -- get property values
   
   /* (non-Javadoc)
    * @see com.christopheragross.bowler.impl.Bowlable#isActive()
    */
   @Override
   public boolean isActive()
   {
      return this.status == Game.kStatusActive;
   }
   
   public boolean isBaker()
   {
      return this.type == Game.kTypeBaker;
   }
   
   /* (non-Javadoc)
    * @see com.christopheragross.bowler.impl.Bowlable#isComplete()
    */
   @Override
   public boolean isComplete()
   {
      return this.status == Game.kStatusComplete;
   }
   
   public boolean isNoTap()
   {
      return this.type == Game.kTypeNoTap;
   }
   
   // ACCESSORS -- set property values
   
   /* (non-Javadoc)
    * @see com.christopheragross.bowler.impl.Bowlable#makeActive()
    */
   @Override
   public void makeActive()
   {
      this.status = Game.kStatusActive;
   }
   
   public void makeBaker()
   {
      this.type = Game.kTypeBaker;
   }
   
   /* (non-Javadoc)
    * @see com.christopheragross.bowler.impl.Bowlable#makeComplete()
    */
   @Override
   public void makeComplete()
   {
      this.status = Game.kStatusComplete;
   }
   
   public void makeNoTap()
   {
      this.type = Game.kTypeNoTap;
   }
   
   // CONDUCT BUSINESS
   
   public void addBowler(Bowler bowler) throws GameException
   {
      
   }
   
   public int calculateScore() 
   {
      return 0;
   }
   
   public void createFrame() throws GameException
   {
      
   }
   
   public void createFrame(String type) throws GameException
   {
      
   }
   
   public void removeBowler(Bowler bowler) throws GameException
   {
      
   }
   
   private class GameRules
   {
      public static final int kMaxBowlers = 10;
      public static final int kMaxFrames = 100;
      
      private int minBowlers;
      private int maxBowlers;
      private int minFrames;
      private int maxFrames;
      
      private GameRules()
      {
         try 
         {
            this.setMaxBowlers(1);
            this.setMinBowlers(1);
            this.setMaxFrames(10);
            this.setMinFrames(10);
         } 
         catch (GameException ge)
         {
            ge.printStackTrace();
         }
      }
      
      public int getMinFrames()
      {
         return this.minFrames;
      }
      
      public int getMaxFrames()
      {
         return this.maxFrames;
      }
      
      public void setMinBowlers(int minBowlers) throws GameException
      {
         if (minBowlers < 1 || minBowlers > GameRules.kMaxBowlers)
         {
            throw new GameException("Minimum bowlers cannot be less than 1");
         }
         
         this.minBowlers = minBowlers;
         
         // update maxBowlers if minBowlers is now larger
         if (this.maxBowlers < this.minBowlers)
         {
            this.setMaxBowlers(this.minBowlers);
         }
      }
      
      public void setMaxBowlers(int maxBowlers) throws GameException
      {
         if (maxBowlers < this.minBowlers || maxBowlers > GameRules.kMaxBowlers)
         {
            throw new GameException("Maximum bowlers must be in range [" +
             this.minBowlers + "," + GameRules.kMaxBowlers + "]");
         }

         this.maxBowlers = maxBowlers;
      }
      
      public void setMinFrames(int minFrames) throws GameException
      {
         if (minFrames < 1 || minFrames > GameRules.kMaxFrames)
         {
            throw new GameException("Minimum frames must be in range [1,100]");
         }
         
         this.minFrames = minFrames;
         
         // update maxFrames if minFrames is now larger
         if (this.maxFrames < this.minFrames)
         {
            this.setMaxFrames(this.minFrames);
         }
      }
      
      public void setMaxFrames(int maxFrames) throws GameException
      {
         if (maxFrames < this.minFrames || maxFrames > GameRules.kMaxFrames)
         {
            throw new GameException("Maximum frames must be in range [" +
             this.minFrames + "," + GameRules.kMaxFrames + "]");
         }
         
         this.maxFrames = maxFrames;
      }
   }
   
}
