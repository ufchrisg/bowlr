package com.christopheragross.bowlr.menu;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.Resources;

import com.christopheragross.bowlr.BallActivity;
import com.christopheragross.bowlr.BowlerActivity;
import com.christopheragross.bowlr.GameActivity;
import com.christopheragross.bowlr.LeagueActivity;
import com.christopheragross.bowlr.R;
import com.christopheragross.bowlr.StatisticsActivity;
import com.christopheragross.bowlr.TournamentActivity;

public class BowlrMenu {

   private List<BowlrMenuItem> menu = new ArrayList<BowlrMenuItem>();

   public BowlrMenu(Context ctxt) {
      Resources res = ctxt.getResources();
      addItem(new BowlrMenuItem(res.getString(R.string.menu_newgame),
            R.drawable.menu_newgame, GameActivity.class));
      addItem(new BowlrMenuItem(res.getString(R.string.menu_bowlers),
            R.drawable.menu_bowlers, BowlerActivity.class));
      addItem(new BowlrMenuItem(res.getString(R.string.menu_balls),
            R.drawable.menu_balls, BallActivity.class));
      addItem(new BowlrMenuItem(res.getString(R.string.menu_leagues),
            R.drawable.menu_leagues, LeagueActivity.class));
      addItem(new BowlrMenuItem(res.getString(R.string.menu_tournaments),
            R.drawable.menu_tournaments, TournamentActivity.class));
      addItem(new BowlrMenuItem(res.getString(R.string.menu_statistics),
            R.drawable.menu_statistics, StatisticsActivity.class));
   }

   private void addItem(BowlrMenuItem item) {
      menu.add(item);
   }

   public List<BowlrMenuItem> getMenu() {
      return this.menu;
   }
}
