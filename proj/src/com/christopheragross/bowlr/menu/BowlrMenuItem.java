package com.christopheragross.bowlr.menu;

public class BowlrMenuItem {

	private String itemName;
	private int itemIconId;
	private Class activityClass;
	
	public BowlrMenuItem(String itemName, int itemIconId, Class activityClass) {
		this.itemName = itemName;
		this.itemIconId = itemIconId;
		this.activityClass = activityClass;
	}
	
	public Class getActivityClass() {
		return this.activityClass;
	}
	
	public String getItemName() {
		return this.itemName;
	}
	
	public int getItemIconId() {
		return this.itemIconId;
	}

	public String toString() {
		return this.itemName;
	}
}
