/**
 * Custom Adapter classes for Lists to be displayed in the Android app
 * 
 * @since 1.0
 * @author Christopher A Gross
 * @version 1.0
 */
package com.christopheragross.bowlr.adapters;