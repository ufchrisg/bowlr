package com.christopheragross.bowlr.adapters;

import java.util.List;

import com.christopheragross.bowlr.R;
import com.christopheragross.bowlr.menu.BowlrMenuItem;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class BowlrMenuItemAdapter extends ArrayAdapter<BowlrMenuItem> {

	private Context ctxt;
	private List<BowlrMenuItem> list;
	
	public BowlrMenuItemAdapter(Context context, int resource,
			List<BowlrMenuItem> objects) {
		super(context, resource, objects);
		this.ctxt = context;
		this.list = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		BowlrMenuItem item = list.get(position);
		
		if (convertView == null) {
			convertView = LayoutInflater.from(ctxt).inflate(R.layout.item_bowlrmenuitem, parent, false);
		}
		
		((ImageView) convertView.findViewById(R.id.menuItemIconView)).setImageResource(item.getItemIconId());
		((TextView) convertView.findViewById(R.id.menuItemTextView)).setText(item.getItemName());
		
		return convertView;
	}
	
}
