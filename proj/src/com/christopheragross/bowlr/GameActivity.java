package com.christopheragross.bowlr;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class GameActivity extends Activity 
{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);
		
		LinearLayout frameContainer = (LinearLayout) findViewById(R.id.frameContainer);
		
		for (int i = 1; i <= 10; i++) {
		   FrameLayout frame = new FrameLayout(this);
		   frameContainer.addView(frame);
		}
	}

	private class FrameLayout extends RelativeLayout {
	   
	   public FrameLayout(Context context) {
	      super(context);
	      
	      View.inflate(context, R.layout.layout_frame, this);
	   }
	   
	}
}
