package com.christopheragross.bowlr;

import java.util.HashMap;

import android.app.Activity;

public abstract class ActivityFactory {
	
	private static HashMap<String, Class> m_RegisteredActivities = 
	 new HashMap<String, Class>();
	
	public static void registerActivity(int activityId, Class activityClass) 
	{
		m_RegisteredActivities.put(""+activityId, activityClass);
	}
	
	public static Activity createActivity(int activityId, Object[] params) 
	 throws InstantiationException, IllegalAccessException 
	{
		Class activityClass = (Class) m_RegisteredActivities.get(""+activityId);
		return (Activity) activityClass.newInstance();
	}

}
