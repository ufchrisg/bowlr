/**
 * API classes and constant enums for Bowling objects.
 * 
 * @since 1.0
 * @author Christopher A Gross
 * @version 1.0
 */
package com.christopheragross.bowlr.api;