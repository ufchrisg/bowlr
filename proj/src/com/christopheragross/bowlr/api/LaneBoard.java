package com.christopheragross.bowlr.api;

public enum LaneBoard
{
   NL20(60, true), NL19(59, true), NL18(58, true), NL17(57, true), 
   NL16(56, true), NL15(55, true), NL14(54, true), NL13(53, true), 
   NL12(52, true), NL11(51, true), NL10(50, true), NL9(49, true), 
   NL8(48, true), NL7(46, true), NL6(45, true), NL5(44, true), 
   NL4(43, true), NL3(42, true), NL2(41, true), NL1(40, true),
   L1(39), L2(38), L3(37), L4(36), L5(35),
   L6(34), L7(33), L8(32), L9(31), L10(30),
   L11(29), L12(28), L13(27), L14(26), L15(25),
   L16(24), L17(23), L18(22), L19(21), L20(20),
   R20(20), R19(19), R18(18), R17(17), R16(16),
   R15(15), R14(14), R13(13), R12(12), R11(11),
   R10(10), R9(9), R8(8), R7(7), R6(6),
   R5(5), R4(4), R3(3), R2(2), R1(1),
   NR1(0, true), NR2(-1, true), NR3(-2, true), NR4(-3, true), 
   NR5(-4, true), NR6(-5, true), NR7(-6, true), NR8(-7, true), 
   NR9(-8, true), NR10(-9, true), NR11(-10, true), NR12(-11, true), 
   NR13(-12, true), NR14(-13, true), NR15(-14, true), NR16(-15, true), 
   NR17(-16, true), NR18(-17, true), NR19(-18, true), NR20(-19, true);
   
   private int board;
   private boolean outOfBounds;
   
   private LaneBoard(int board)
   {
      this(board, false);
   }
   
   private LaneBoard(int board, boolean outOfBounds)
   {
      this.board = board;
      this.outOfBounds = outOfBounds;
   }
   
   /**
    * Gets the int value of this @LaneBoard - for graph
    *  calculation purposes.
    * @return the int value of this @LaneBoard
    */
   public int getIntValue()
   {
      return this.board;
   }
   
   /**
    * Checks if this @LaneBoard is beyond the boundary of the Lane. 
    *  Out of bounds boards are allowed for certain properties, such as 
    *  the stance board since a Bowler can stand beyond the lane 
    *  boundaries and still throw a legal shot.
    * @return true if this board is out of bounds
    */
   public boolean isOutOfBounds()
   {
      return this.outOfBounds;
   }
}
