package com.christopheragross.bowlr.api;

import com.christopheragross.bowlr.exception.ShotException;

/**
 * The AdvancedShot interface. AdvancedShot objects track more detailed
 *  properties of a Shot. In particular, these objects track properties
 *  of this Shot that pertain to where things occur during the Shot. These
 *  details are used to create a graphical representation of the bowler's Shot
 *  within the app.
 *  
 * @since 1.0
 * @author Chris
 * @version 1.0
 */
public interface AdvancedShot extends Shot
{
   /**
    * Get the break point board targetted for this Shot.
    * @return break point board targetted
    */
   public Integer getBreakPointBoard();
   
   /**
    * Get the break point board hit for this Shot.
    * @return break point board hit
    */
   public Integer getBreakPointHitBoard();
   
   /**
    * Get the entry point board for this Shot.
    * @return board where the ball first made contact with pins
    */
   public Integer getEntryPointBoard();
   
   /**
    * Get the board at the foul line where the Bowler finished 
    *  his or her approach for this Shot. If the internal value is
    *  not set then it will return the value for stance board if it
    *  is set.
    * @return board at foul line where sliding foot was 
    */
   public Integer getFoulLineStanceBoard();
   
   /**
    * Get the board where the Bowler started his or her approach
    *  for this Shot.
    * @return board where approach began
    */
   public Integer getStanceBoard();
   
   /**
    * Get the targetted board at the arrows on the lane for this Shot.
    * @return board at arrows this Shot targetted
    */
   public Integer getTargetBoard();
   
   /**
    * Get the board that was hit at the arrows on the lane for this Shot.
    * @return board at arrows hit for this Shot
    */
   public Integer getTargetHitBoard();
   
   /**
    * Set the targetted break point board for this Shot. Only boards that
    *  exist on the lane are valid values. One real board is equal to two
    *  here to account for half boards and to keep storage requirements low.
    *  Valid values are in the range [79, 156].
    * @param laneBoard break point board targetted
    * @throws ShotException if the board provided is not a valid value
    */
   public void setBreakPointBoard(Integer laneBoard) throws ShotException;
   
   /**
    * Set the break point board hit for this Shot. Only boards that exist
    *  on the lane are valid values. One real board is equal to two
    *  here to account for half boards and to keep storage requirements low.
    *  Valid values are in the range [79, 156].
    * @param laneBoard break point board hit
    * @throws ShotException if the board provided is not a valid value
    */
   public void setBreakPointHitBoard(Integer laneBoard) throws ShotException;
   
   /**
    * Set the entry point board hit for this Shot. Only boards that exist
    *  on the lane are valid values. One real board is equal to two
    *  here to account for half boards and to keep storage requirements low.
    *  Valid values are in the range [79, 156].
    * @param laneBoard entry point board hit
    * @throws ShotException if the board provided is not a valid value
    */
   public void setEntryPointBoard(Integer laneBoard) throws ShotException;
   
   /**
    * Set the board where the Bowler ended his or her approach at/near
    *  the foul line. The approach can be outside the bounds of the lane
    *  and therefore have a much wider range that is equal to three times
    *  the standard range. One real board is equal to two here to account 
    *  for half boards and to keep storage requirements low. Valid values 
    *  are in the range [1, 234].
    * @param laneBoard stance board at foul line
    * @throws ShotException if the board provided is not a valid value
    */
   public void setFoulLineStanceBoard(Integer laneBoard) throws ShotException;
   
   /**
    * Set the board where the Bowler began his or her approach. The approach
    *  can be outside the bounds of the lane and therefore can have a much
    *  wider range that is equal to three times the standard range. One real
    *  board is equal to two here to account for half boards and to keep
    *  storage requirements low. Valid values are in the range [1, 234].
    * @param laneBoard stance board
    * @throws ShotException if the board provided is not a valid value
    */
   public void setStanceBoard(Integer laneBoard) throws ShotException;
   
   /**
    * Set the board targetted for this Shot. Since bowlers that loft may
    *  still get a ball beyond the arrows before it touches the lane the
    *  valid range will be 1.5x the size of a lane. Valid values are in
    *  the range [39, 156].
    * @param laneBoard board targetted
    * @throws ShotException if the board provided is not a valid value
    */
   public void setTargetBoard(Integer laneBoard) throws ShotException;
   
   /**
    * Set the board hit at the arrows for this Shot. Since bowlers that loft
    *  may still get a ball beyond the arrows before it touches the lane the
    *  valid range will be 1.5x the size of a lane. Valid values are in
    *  the range [39, 156].
    * @param laneBoard board hit at arrows
    * @throws ShotException if the board provided is not a valid value
    */
   public void setTargetHitBoard(Integer laneBoard) throws ShotException;
}
