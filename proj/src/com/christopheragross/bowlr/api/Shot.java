package com.christopheragross.bowlr.api;

/**
 * The Shot interface. Shot objects track the "who" and "what" of a Shot
 *  within a Frame.
 *  
 * @since 1.0
 * @author Chris
 * @version 1.0
 */
public interface Shot extends Bowlable
{ 
   /**
    * Get the current PinDeck object.
    * @return the current PinDeck
    */
   public PinDeck getPinDeck();
   
   /**
    * Get the value of this Shot based on the difference between
    *  the original PinDeck and the current PinDeck.
    * @return the value of this shot in the range [0, 10]
    */
   public int getShotValue();
   
   /**
    * Check if this Shot has been marked as a foul.
    * @return true if this Shot is a foul.
    */
   public boolean isFoul();
   
   /**
    * Mark this Shot as a foul.
    */
   public void markFoul();
   
   /**
    * Reset this Shot to its original values. This will call a copy
    *  constructor of Shot that passes in either the previous Shot if
    *  this is a SecondShot or a new Shot otherwise.
    */
   public void resetShot();
   
   /**
    * Unmark this Shot as a foul.
    */
   public void unMarkFoul();
}
