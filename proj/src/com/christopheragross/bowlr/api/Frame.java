package com.christopheragross.bowlr.api;

import java.util.List;

import com.christopheragross.bowlr.exception.FrameException;

/**
 * The Frame interface. Frame objects track the "who" and "what" of a Frame
 *  within a Game.
 *  
 * @since 1.0
 * @author Chris
 * @version 1.0
 */
public interface Frame extends Bowlable
{
   /**
    * Adds a Shot to this Frame.
    * @param aShot the Shot to add
    * @throws FrameException if adding a Shot breaks a rule
    */
   public void addShot(Shot aShot) throws FrameException;
   
   /**
    * Factory method for creating a new Frame of a given FrameType
    * @param The FrameType to use when creating the new Frame
    * @return The Frame created
    */
   public Frame createFrame(FrameType type);
   
   /**
    * Get the Bowler objects associated with this Frame
    * @return a List of Bowler objects
    */
   public List<Bowler> getBowlers();
   
   /**
    * Get the Shot objects associated with this Frame
    * @return a List of Shot objects
    */
   public List<Shot> getShots();
}
