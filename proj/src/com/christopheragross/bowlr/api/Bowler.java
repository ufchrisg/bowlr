package com.christopheragross.bowlr.api;

import com.christopheragross.bowlr.exception.BowlerException;

/**
 * Bowler interface that defines what a Bowler object is within the 
 *  scope of this app.
 *  
 * @since 1.0
 * @author Chris
 * @version 1.0
 */
public interface Bowler
{
   // static constant for the default bowler name
   public static final String kDefaultName = "Bowler";

   /**
    * Enum for handedness constants. This is bowler-specific so placing
    *  within the Bowler interface vs in own class
    */
   public enum Handedness
   {
      LEFT, RIGHT, TWO
   }
   
   // ACCESSORS -- get properties
   /**
    * Get the member association id for this Bowler
    * @return the member association id for this bowler
    */
   public String getMemberId(); 
   
   /**
    * Get the name of this Bowler
    * @return the name of this bowler
    */
   public String getName();
   
   // ACCESSORS -- get property values
   /**
    * Check if this Bowler is left-handed
    * @return true if Bowler is left-handed
    */
   public boolean isLefty();
   
   /**
    * Check if this Bowler is right-handed
    * @return true if Bowler is right-handed
    */
   public boolean isRighty();
   
   /**
    * Check is this Bowler is two-handed
    * @return true if Bowler is two-handed
    */
   public boolean isTwoHand();
   
   // ACCESSORS -- set property values
   /**
    * Make this Bowler left-handed
    */
   public void makeLefty();
   
   /**
    * Make this bowler right-handed
    */
   public void makeRighty();
   
   /**
    * Make this bowler 2-handed
    */
   public void makeTwoHand();
   
   /**
    * Sets member id to null if not already
    */
   public void removeMemberId();
   
   /**
    * Set a value for the memberId
    * @param newMemberId
    */
   public void setMemberId(String newMemberId) throws BowlerException;
   
   // CONDUCT BUSINESS
   /**
    * Changes the name of this Bowler.
    * @param newName
    * @throws BowlerException if the name is empty or null
    */
   public void setName(String newName) throws BowlerException;
}
