package com.christopheragross.bowlr.api;

public interface Storable
{
   // ACCESSORS -- get property values
   /**
    * Check if this object can be written to persistent storage. True should
    *  be the default value for all objects that implement this.
    * @return true if this object can be written to persistent storage
    */
   public boolean isDbWriteEnabled();
   
   // ACCESSORS -- set property values
   /**
    * Enable writing of this object's data to persistent storage.
    */
   public void enableDbWrite();
   
   /**
    * Disable writing of this object's data to persistent storage.
    */
   public void disableDbWrite();
}
