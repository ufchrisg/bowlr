package com.christopheragross.bowlr.api;

/**
 * The FirstShot interface. Only FirstShot objects can call the
 *  {@link #markStrike()} method.
 *  
 * @since 1.0
 * @author Chris
 * @version 1.0
 */
public interface FirstShot extends Shot
{
   /**
    * Mark this Shot as a strike. This calls the
    * {@link PinDeck#clearDeck() PinDeck.clearDeck()} method.
    */
   public void markStrike();
}
