package com.christopheragross.bowlr.api;

/**
 * GameType enum. This contains all values and some business rule checks
 *  for valid types of bowling games.
 *  
 * @since 1.0
 * @author Chris
 * @version 1.0
 */
public enum GameType
{
   STANDARD; 
//   BAKER(10, 2, 10, new ArrayList<Integer>()),
//   NOTAP(10, 1, 1, new ArrayList<Integer>()), 
//   THREESIXNINE(10, 1, 1, Arrays.asList(3, 6, 9));
   
   private final int numFrames;
//   private final int minBowlers;
//   private final int maxBowlers;
//   private final List<Integer> autoStrikeFrames;
   
   private GameType()
   {
      this.numFrames = 10;
//      this.minBowlers = 1;
//      this.maxBowlers = 1;
//      this.autoStrikeFrames = new ArrayList<Integer>();
   }
   
//   private GameType(
//      int numFrames, 
//      int minBowlers, 
//      int maxBowlers,
//      List<Integer> autoStrikeFrames)
//   {
//      this.numFrames = numFrames;
//      this.minBowlers = minBowlers;
//      this.maxBowlers = maxBowlers;
//      this.autoStrikeFrames = autoStrikeFrames;
//   }
//   
//   /**
//    * Checks if rules allow for a new @Bowler to be added to passed @List
//    * @param bowlers
//    * @return true if adding a @Bowler would not break any rules
//    */
//   public boolean canAddBowler(Game aGame)
//   {
//      return (this.maxBowlers > aGame.getBowlers().size());
//   }
//   
//   /**
//    * Checks if rules allow for a @Bowler to be removed from passed @List
//    * @param bowlers
//    * @return true if removing a @Bowler would not break any rules
//    */
//   public boolean canRemoveBowler(Game aGame)
//   {
//      return (this.minBowlers < aGame.getBowlers().size());
//   }
   
   /**
    * Checks if rules allow for a new @Frame to be added to passed @List
    * @param frames
    * @return true if adding a @Frame would not break any rules
    */
   public boolean canAddFrame(Game aGame)
   {
      return (this.numFrames > aGame.getFrames().size());
   }
   
//   /**
//    * Checks if this @GameType has auto strike frames
//    * @return true if this @GameType has auto strike frames
//    */
//   public boolean hasAutoStrikeFrames()
//   {
//      return !this.autoStrikeFrames.isEmpty();
//   }
//   
//   /**
//    * Check if the passed @Frame should be an automatic strike
//    * @param frameNumber
//    * @return true if the @Frame should be a strike
//    */
//   public boolean isAutoStrike(Frame aFrame)
//   {
//      return this.autoStrikeFrames.contains(aFrame.getFrameNumber());
//   }
}