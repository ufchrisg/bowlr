package com.christopheragross.bowlr.api;

/**
 * Pin constants enum.
 * 
 * A pin has a row and column value that corresponds to the following
 *  map that is a pin deck:
 *  
 *    | 0  1  2  3  4  5  6
 * --------------------------------
 *  0 |          1
 *  1 |       2     3
 *  2 |    4     5     6
 *  3 | 7     8     9     10
 *  
 *  These values are used by the PinDeck to determine if the current
 *  pin layout is a split.
 *  
 * @since 1.0 
 * @author Christopher A Gross
 * @version 1.0
 */
public enum Pin
{
   ONE(0, 3),
   TWO(1, 2),
   THREE(1, 4),
   FOUR(2, 1),
   FIVE(2, 3),
   SIX(2, 5),
   SEVEN(3, 0),
   EIGHT(3, 2),
   NINE(3, 4),
   TEN(3, 6);
   
   private final int row;
   private final int column;
   
   private Pin(int row, int column)
   {
      this.row = row;
      this.column = column;
   }
   
   /**
    * Get the row number for this Pin.
    * @return the row number
    */
   public int getRow()
   {
      return this.row;
   }
   
   /**
    * Get the column number for this Pin.
    * @return the column number
    */
   public int getColumn()
   {
      return this.column;
   }
}
