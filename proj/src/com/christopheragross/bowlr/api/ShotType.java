package com.christopheragross.bowlr.api;

public enum ShotType
{
   FIRST,
   SECOND;
}
