package com.christopheragross.bowlr.api;

import com.christopheragross.bowlr.exception.PinDeckException;

/**
 * The PinDeck interface. PinDeck objects reflect the state of the
 *  10 pins at a current time. This state is stored for each shot and
 *  reset for proceeding frames.
 *  
 * PinDeck objects are initialized with a full rack of standing Pin objects.
 *  
 * @since 1.0
 * @author Chris
 * @version 1.0
 */
public interface PinDeck
{
   /**
    * Set the state of all pins on this PinDeck to "Down".
    */
   public void clearDeck();
   
   /**
    * Count the number of pins currently marked as "Down".
    * @return number of downed pins
    */
   public int countPinsDown();
   
   /**
    * Count the number of pins currently marked as "Up".
    * @return number of standing pins
    */
   public int countPinsStanding();
   
   /**
    * Checks if the specified Pin is down.
    * @param aPin The Pin to check
    * @return true of the Pin is down
    */
   public boolean isDown(Pin aPin);
   
   /**
    * Determines if the current state of the PinDeck can be classified
    *  as a split. Splits are any pin configuration where there is a gap
    *  of at least one pin between 2 or more pins and the head pin (Pin #1) 
    *  is not standing.
    * @return true if the current PinDeck is a split
    */
   public boolean isSplit();
   
   /**
    * Checks if the specified Pin is standing.
    * @param aPin The Pin to check
    * @return true if the Pin is standing
    */
   public boolean isStanding(Pin aPin);
   
   /**
    * Mark a Pin as "Down".
    * @param aPin The Pin to mark as "Down".
    * @throws PinDeckException if the specified Pin does not exist for
    *  this PinDeck
    */
   public void markPinDown(Pin aPin) throws PinDeckException;
   
   /**
    * Mark a Pin as "Up".
    * @param aPin The Pin to mark as "Up".
    * @throws PinDeckException if the specified Pin does not exist for
    *  this PinDeck
    */
   public void markPinStanding(Pin aPin) throws PinDeckException;
   
   /**
    * Set the state of all pins on this PinDeck to "Up".
    */
   public void resetDeck();
}
