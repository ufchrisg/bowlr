package com.christopheragross.bowlr.api;

/**
 * FrameType enum. This contains all values and some business rule checks
 *  for valid types of frames.
 *  
 * @since 1.0
 * @author Chris
 * @version 1.0
 */
public enum FrameType
{
   STANDARD(2),
   FINAL(3);
   
   private final int numShots;
   
   private FrameType(int numShots)
   {
      this.numShots = numShots;
   }
  
   /**
    * Checks if a Frame of this FrameType has space for another Shot.
    * @param aFrame The Frame we want to add a Shot to
    * @return true if there is space for a Shot in the passed Frame
    */
   public boolean canAddShot(Frame aFrame)
   {
      return (this.numShots > aFrame.getShots().size());
   }
}
