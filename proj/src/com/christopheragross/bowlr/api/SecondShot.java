package com.christopheragross.bowlr.api;

/**
 * The SecondShot interface. Only SecondShot objects can call the
 *  {@link #markSpare()} method.
 * @author Chris
 *
 */
public interface SecondShot extends Shot
{
   /**
    * Mark this Shot as a spare. This calls the 
    * {@link PinDeck#clearDeck() PinDeck.clearDeck()} method.
    */
   public void markSpare();
}
