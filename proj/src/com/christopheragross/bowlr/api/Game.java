package com.christopheragross.bowlr.api;

import java.util.List;

import com.christopheragross.bowlr.exception.GameException;

/**
 * The Game interface. Game objects track the "who" and "what" of a Game.
 *  
 * @since 1.0
 * @author Chris
 * @version 1.0
 */
public interface Game extends Bowlable
{
   /**
    * Adds a Frame to this Game
    * @param aFrame the Frame to add
    * @throws GameException if adding a Frame breaks a rule
    */
   public void addFrame(Frame aFrame) throws GameException;
   
   /**
    * Get a List containing the Bowler objects attached to this Game
    * @return List of Bowler objects
    */
   public List<Bowler> getBowlers();
   
   /**
    * Get a List containing the Frame objects attached to this Game
    * @return List of Frame objects
    */
   public List<Frame> getFrames();
   
//   /**
//    * Checks if this Game is a Baker game, which is a game that
//    *  alternates between a List of Bowler objects that complete
//    *  each Frame
//    * @return true if this is a Baker Game.
//    */
//   public boolean isBakerGame();
//   
//   /**
//    * Checks if this Game is No Tap game, which is a game that allows
//    *  for counts less than maximum to be a Strike.
//    * @return true if this is a No Tap Game
//    */
//   public boolean isNoTapGame();
//   
//   /**
//    * Checks if this Game allows for some frames to be automatic strikes
//    * @return true if this is an auto strike Game
//    */
//   public boolean isAutoStrikeGame();
   
   /**
    * Attempts to remove a Bowler from this Game
    * @param aBowler the Bowler to remove
    * @throws GameException if removing this Bowler breaks a rule
    */
   public void removeBowler(Bowler aBowler) throws GameException;
   
   /**
    * Attempts to remove a Frame from this Game
    * @param aFrame the Frame to remove
    * @throws GameException if removing this Frame breaks a rule
    */
   public void removeFrame(Frame aFrame) throws GameException; 
   
}
