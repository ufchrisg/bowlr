package com.christopheragross.bowlr.api;

import com.christopheragross.bowlr.exception.BowlableException;

public interface Bowlable
{
   /**
    * Adds a Bowler to this Bowlable object. Each implementing class will
    *  handle its own business rule checking.
    *  
    * @param aBowler
    */
   public void addBowler(Bowler aBowler);
   
   /**
    * Check if this object is active
    * @return true if this object is active
    */
   public boolean isActive();

   /**
    * Check if this object is complete
    * @return true if this object is complete
    */
   public boolean isComplete();

   /**
    * Mark this object as active. Only one object at a given level can
    *  be active at a time. e.g. Only one Game can be active of a number
    *  of available Game objects but a Frame within that Game can also be
    *  active. A Frame can only be active within a Game that is active and
    *  also only one Frame of that Game can be active at any given time.
    *  
    *  Active objects will be the first loaded if the user changes to
    *  another app/screen and then returns. The order searched will be 
    *   1) Active Game
    *   2) Active Frame within Game
    *   3) Active Shot within Frame
    */
   public void makeActive() throws BowlableException;
   
   /**
    * Mark this object as inactive.
    */
   public void makeInActive();

   /**
    * Mark this object as complete. This cannot be undone as being
    *  "complete" will trigger saving data to persistent storage. Any
    *  changes to the object after being marked complete will trigger
    *  storage saves after each edit.
    */
   public void makeComplete();

}